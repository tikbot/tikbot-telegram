from telegram.ext import ContextTypes


async def start_handler(update, context: ContextTypes.DEFAULT_TYPE):
    await update.effective_message.reply_text(
        "Hey! \U0001f44b\U0001f3fd Add me to a group, send me a private message or mention me a TikTok share link and I'll reply with the actual video!"
    )
