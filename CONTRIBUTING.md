# Contributing

If you have an idea for a feature or want to contribute code, feel free to do so via GitLab.

## Development setup

### Dependencies

- Python > 3.8 is required
- Install [poetry](https://python-poetry.org/) (`pip install poetry`)
- Create the virtualenv and install required dependencies with `poetry install`

### Backend API

For documentation on the API, refer to the [tikbot-api repository](https://gitlab.com/tikbot/tikbot-api/).

### Services

You need a function Redis server, this can be accomplished via [Docker](https://hub.docker.com/_/redis):

- `docker run -d -p 6379:6379 redis`

### Environment variables

Refer to the [.env.example](.env.example) file in this repository for an overview over the required environment variables.

## Running

- `python3 tikbot_telegram/main.py`
